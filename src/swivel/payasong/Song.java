package swivel.payasong;

/**
 * Created by  Sampath on 3/31/2020
 * Song details implements
 */
public class Song {
    String name;
    Double rate;

    /**
     * Create new song by initializing.
     * @param name name of the song
     * @param rate rate of the song
     */
    public Song(String name, Double rate){
        this.name = name;
        this.rate = rate;
    }

}
