package swivel.payasong;

/**
 * Created by  Sampath on 3/31/2020
 * Reprecenting users of the system.
 *
 */
public class User {
    private String name;
    private String email;

    /**
     * Create new user by initializing.
     * @param name name of the user
     * @param email email of the user
     */
    public User(String name, String email){
        this.name = name;
        this.email = email;
    }

    /**
     * Getter of the name property
     */
    public String getName(){
        return this.name;
    }

    /**
     * Getter of the email property
     */
    public String getEmail(){
        return email;
    }

    /**
     * Display user information
     * This method will override by type of user classes
     */
    public void printDetails(){};
}
