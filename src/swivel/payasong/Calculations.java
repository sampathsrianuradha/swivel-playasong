package swivel.payasong;

/**
 * Created by Sampath 3/31/2020
 * Calculation interface
 */
public interface Calculations {
    void calculateRevenue(Artist artist);
}
