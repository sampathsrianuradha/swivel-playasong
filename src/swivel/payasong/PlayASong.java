package swivel.payasong;

/**
 * Created by Sampath 3/31/2020
 * Play a song class represents the interface of the application
 */
public class PlayASong {
    public static void main(String[] args){
        try{
            Artist artist = new Artist("Pabilo Albo", "pablo@gmail.com", "pop");
            Operator operator = new Operator("Simon", "simon@playasong.com", 123, "accountant");

            artist.addSongs();
            artist.printDetails();
            artist.printSongsList();
            operator.printDetails();
            operator.calculateRevenue(artist);

        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
