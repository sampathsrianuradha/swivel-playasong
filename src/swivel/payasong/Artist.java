package swivel.payasong;

import java.util.Scanner;

/**
 * Created by  Sampath on 3/31/2020
 * Artist of the user interaction implement in the Artist class
 * Extends by User class
 */
public class Artist extends User {
    public static final Integer NO_OF_SONGS = 5;
    public String genre;
    public String[][] songList;

    /**
     * Create new Artist user by initializing.
     *
     * @param name  name of the user
     * @param email email of the user
     */
    public Artist(String name, String email, String genre) {
        super(name, email);
        this.genre = genre;
    }

    /**
     * Override User printDetails method
     */
    @Override
    public void printDetails() {
        super.printDetails();
        System.out.println("Artist name is " + super.getName());
        System.out.println("Artist email is " + super.getEmail());
        System.out.println("Artist music genre is " + this.genre);
        System.out.println("Number of songs is " + NO_OF_SONGS);
    }

    /**
     * Add songs to the array
     */
    public void addSongs(){
        this.songList = new String[NO_OF_SONGS][2];
        for(int i = 0; i < NO_OF_SONGS; i++){
            for(int j = 0; j < this.songList[i].length; j++) {
                Scanner input = new Scanner(System.in);
                switch (j){
                    case 0:
                        System.out.println("Enter song:");
                        while (!input.hasNext()){
                            System.out.println("Please enter song name:");
                            input.next();
                        }
                        this.songList[i][0] = input.nextLine();
                        break;
                    case 1:
                        System.out.println("Enter rate:");
                        while (!input.hasNextDouble()){
                            System.out.println("Please enter valid song rate:");
                            input.next();
                        }
                        double rate = input.nextDouble();
                        this.songList[i][1] = Double.toString(rate);
                        break;
                }
            }
            System.out.println("");
        }
    }

    /**
     * Printing songs list as tab formatted
     */
    public void printSongsList(){
        System.out.println("Song list is:");
        for (int i = 0; i < this.songList.length; i++){
            System.out.println(this.songList[i][0] + "\t"+ this.songList[i][1]);
        }
        System.out.println("");
    }
}
