package swivel.payasong;

import java.util.Scanner;

/**
 * Created by  Sampath on 3/31/2020
 * Operator of the user interaction implement in the Operator class
 * Extends by User class
 */
public class Operator extends User implements Calculations{
    private int employeeNumber;
    private String jobTitle;

    /**
     * Create new operator  user by initializing.
     *
     * @param name  name of the user
     * @param email email of the user
     * @param employeeNumber employee number of the operator
     * @param jobTitle job tile of the operator
     */
    public Operator(String name, String email, Integer employeeNumber, String jobTitle) {
        super(name, email);
        this.employeeNumber = employeeNumber;
        this.jobTitle = jobTitle;
    }

    /**
     * Override User printDetails method
     */
    @Override
    public void printDetails() {
        super.printDetails();
        System.out.println("Operator name is " + super.getName());
        System.out.println("Operator email is " + super.getEmail());
        System.out.println("Operator employee number is " + this.employeeNumber);
        System.out.println("Operator designation is " + this.jobTitle);
        System.out.println("");
    }


    @Override
    public void calculateRevenue(Artist artist) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter number of downloads: ");
        while (!input.hasNextInt()){
            System.out.println("Please enter valid number of downloads:");
            input.next();
        }
        int noOfDownloads = input.nextInt();
        System.out.println("");
        double totalRate = calculateTotalRate( artist.songList );
        double averageRate = calculateAverageRate(totalRate, artist.songList.length);
        double revenue = averageRate * noOfDownloads;
        System.out.println("Artist: " + artist.getName());
        System.out.println("Album revenue is LKR " + revenue);
    }

    /**
     * Get total rate of the song list
     */
    public double calculateTotalRate(String[][] songList){
        double totalRate = 0.0;
        for (int i = 0; i < songList.length; i++) {
            totalRate += Double.valueOf( songList[i][1]);
        }
        return totalRate;
    }

    /**
     * Calculate average rate
     */
    public double calculateAverageRate(Double totalRate, Integer noOfSongs){
        return totalRate / noOfSongs;
    }
}
